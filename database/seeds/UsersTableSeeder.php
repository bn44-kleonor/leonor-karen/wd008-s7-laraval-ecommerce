<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use App\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
     	User::insert([
     		['username' => 'admin', 'enum' => 'admin', 'email' => 'admin@gmail.com', 'password' => Hash::make('admin123')],
     		['username' => 'lalen', 'enum' => 'user', 'email' => 'lalen@gmail.com', 'password' => Hash::make('lalen123')]
     	])
    }
}
