<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    //many Products "belongs to many" Orders (MANY-to-MANY)
    public function orders()
    {
        return $this->belongsToMany('App\Product');
    }
}
